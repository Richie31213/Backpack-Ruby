# Backpack Ruby

## Description

This script allows the user to download certain programs easily. It is made for Arch based Linux distros since it pulls from the AUR. These programs are some that I find very useful such as PyCharm and Ranger.

## Usage

To be able to run this you **MUST** have 2 things already installed on your computer: Ruby and the Colorize gem.

To check if you have ruby installed:
```
ruby --version
```
To check if you have colorize installed:
```
gem list
```
If you have colorize installed it will show up in the list.

To install them:
```
ruby: sudo pacman -S ruby
colorize: gem install colorize
```
To get the script:
```
git clone https://gitlab.com/Richie31213/Backpack-Ruby.git
```

To run the script:
```
ruby Backpack-Ruby.rb
```

## WARNINGS

```diff
- NO CONFIRM IS SET ON BY DEFAULT
- To exit out of the script when no confirm is OFF, type q, press enter, then type n
```
