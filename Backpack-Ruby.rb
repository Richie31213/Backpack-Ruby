#! /usr/bin/ruby
if(Process.uid == 1000)
    puts "Please run as root\n"
else
    require 'colorize'

    $cont = true
    $noConf = true

    def printPrograms()
    puts "            Table of Contents            \n"
    puts "-----------------------------------------\n"
    puts "| 1. Atom                               |\n"
    puts "| 2. Git                                |\n"
    puts "| 3. Discord                            |\n"
    puts "| 4. Pycharm                            |\n"
    puts "| 5. Htop                               |\n"
    puts "| 6. Gparted                            |\n"
    puts "| 7. Pcmanfm                            |\n"
    puts "| 8. Ranger                             |\n"
    puts "| 9. Spotify                            |\n"
    puts "-----------------------------------------\n"
    end

    puts "Welcome to the backpack of starter applications for Arch!\n".yellow

    if(File.file?('/usr/bin/yay') == false)
    puts "Installing Dependencies"
    if(File.file?('/usr/bin/yay'))
        system('sudo pacman -S --noconfirm yay')
    end
    else
    puts "Dependencies met"
    end

    printPrograms
    while($cont)
    while($noConf)
        puts "\nEnter the number of the program you would like to install or 'h' for help: "
        puts "WARNING:".light_red.blink.on_black
        puts "---------------------".light_red.on_black
        puts "--noconfirm is ON.".light_red.on_black
        puts "To toggle it type 'N'".light_red.on_black

        input = gets.chomp
        puts " \n"

        if("#{input}" == "1" && File.file?('/usr/bin/atom') == false)
        puts "Installing Atom"
        system('yay -S --noconfirm atom')
        end
        if("#{input}" == "1" && File.file?('/usr/bin/atom'))
        puts "You already have Atom".light_magenta.bold
        end

        if("#{input}" == "2" && File.file?('/usr/bin/git') == false)
        puts "Installing Git"
        system('yay -S --noconfirm git')
        end
        if("#{input}" == "2" && File.file?('/usr/bin/git'))
        puts "You already have Git".light_magenta.bold
        end

        if("#{input}" == "3" && File.file?('/usr/bin/discord') == false)
        puts "Installing GPG keys"
        system('gpg --recv-keys 0fC3042E345AD05D')
        if(File.file?('/usr/lib/libc++.so') == false)
            puts "Installing libc++"
            system('yay -S --noconfirm libc++')
        end
        puts "Installing Discord"
        system('yay -S --noconfirm discord')
        end
        if("#{input}" == "3" && File.file?('/usr/bin/discord'))
        puts "You already have Discord".light_magenta.bold
        end

        if("#{input}" == "4" && File.file?('/usr/bin/pycharm') == false)
        puts "Installing Pycharm"
        system('yay -S --noconfirm pycharm')
        end
        if("#{input}" == "4" && File.file?('/usr/bin/pycharm'))
        puts "You already have Pycharm".light_magenta.bold
        end

        if("#{input}" == "5" && File.file?('/usr/bin/htop') == false)
        puts "Installing Htop"
        system('yay -S --noconfirm htop')
        end
        if("#{input}" == "5" && File.file?('/usr/bin/htop'))
        puts "You already have Htop".light_magenta.bold
        end

        if("#{input}" == "6" && File.file?('/usr/bin/gparted') == false)
        puts "Installing Gparted"
        system('yay -S --noconfirm gparted')
        end
        if("#{input}" == "6" && File.file?('/usr/bin/gparted'))
        puts "You already have Gparted".light_magenta.bold
        end

        if("#{input}" == "7" && File.file?('/usr/bin/pcmanfm') == false)
        puts "Installing Pcmanfm"
        system('yay -S --noconfirm pcmanfm')
        end
        if("#{input}" == "7" && File.file?('/usr/bin/pcmanfm'))
        puts "You already have Pcmanfm".light_magenta.bold
        end

        if("#{input}" == "8" && File.file?('/usr/bin/ranger') == false)
        puts "Installing Ranger"
        system('yay -S --noconfirm ranger')
        end
        if("#{input}" == "8" && File.file?('/usr/bin/ranger'))
        puts "You already have Ranger".light_magenta.bold
        end

        if("#{input}" == "9" && File.file?('/usr/bin/spotify') == false)
        puts "Installing Spotify"
        system('yay -S --noconfirm spotify')
        end
        if("#{input}" == "9" && File.file?('/usr/bin/spotify'))
        puts "You already have spotify".light_magenta.bold
        end

        if("#{input}" == "h" || "#{input}" == "H")
        puts "S: Show the table of contents\n"
        puts "H: Show help menu\n"
        puts "N: Turn off --noconfirm"
        puts "Q: Quit out of the program\n"
        end
        if("#{input}" == "s" || "#{input}" == "S")
        printPrograms
        end
        if("#{input}" == "n" || "#{input}" == "N")
        $noConf = false
        end
        if("#{input}" == "q" || "#{input}" == "Q")
        $cont = false
        break
        end
    end
    while($noConf == false)
        puts "\nEnter the number of the program you would like to install or 'h' for help: "
        puts "WARNING:".light_green.blink.on_black
        puts "---------------------".light_green.on_black
        puts "--noconfirm is OFF.".light_green.on_black
        puts "To toggle it type 'N'".light_green.on_black

        input = gets.chomp
        puts "\n"

        if("#{input}" == "1" && File.file?('/usr/bin/atom') == false)
        puts "Installing Atom"
        system('yay -S atom')
        end
        if("#{input}" == "1" && File.file?('/usr/bin/atom'))
        puts "You already have Atom".light_magenta.bold
        end

        if("#{input}" == "2" && File.file?('/usr/bin/git') == false)
        puts "Installing Git"
        system('yay -S git')
        end
        if("#{input}" == "2" && File.file?('/usr/bin/git'))
        puts "You already have Git".light_magenta.bold
        end

        if("#{input}" == "3" && File.file?('/usr/bin/discord') == false)
        puts "Installing GPG keys"
        system('gpg --recv-keys 0fC3042E345AD05D')
        if(File.file?('/usr/lib/libc++.so') == false)
            puts "Installing libc++"
            system('yay -S libc++')
        end
        puts "Installing Discord"
        system('yay -S discord')
        end
        if("#{input}" == "3" && File.file?('/usr/bin/discord'))
        puts "You already have Discord".light_magenta.bold
        end

        if("#{input}" == "4" && File.file?('/usr/bin/pycharm') == false)
        puts "Installing Pycharm"
        system('yay -S pycharm')
        end
        if("#{input}" == "4" && File.file?('/usr/bin/pycharm'))
        puts "You already have Pycharm".light_magenta.bold
        end

        if("#{input}" == "5" && File.file?('/usr/bin/htop') == false)
        puts "Installing Htop"
        system('yay -S htop')
        end
        if("#{input}" == "5" && File.file?('/usr/bin/htop'))
        puts "You already have Htop".light_magenta.bold
        end

        if("#{input}" == "6" && File.file?('/usr/bin/gparted') == false)
        puts "Installing Gparted"
        system('yay -S gparted')
        end
        if("#{input}" == "6" && File.file?('/usr/bin/gparted'))
        puts "You already have Gparted".light_magenta.bold
        end

        if("#{input}" == "7" && File.file?('/usr/bin/pcmanfm') == false)
        puts "Installing Pcmanfm"
        system('yay -S pcmanfm')
        end
        if("#{input}" == "7" && File.file?('/usr/bin/pcmanfm'))
        puts "You already have Pcmanfm".light_magenta.bold
        end

        if("#{input}" == "8" && File.file?('/usr/bin/ranger') == false)
        puts "Installing Ranger"
        system('yay -S ranger')
        end
        if("#{input}" == "8" && File.file?('/usr/bin/ranger'))
        puts "You already have Ranger".light_magenta.bold
        end

        if("#{input}" == "9" && File.file?('/usr/bin/spotify') == false)
        puts "Installing Spotify"
        system('yay -S spotify')
        end
        if("#{input}" == "9" && File.file?('/usr/bin/spotify'))
        puts "You already have spotify".light_magenta.bold
        end

        if("#{input}" == "h" || "#{input}" == "H")
        puts "S: Show the table of contents\n"
        puts "H: Show help menu\n"
        puts "N: Turn off --noconfirm"
        puts "Q: Quit out of the program (Type Q once then another letter to escape)\n"
        end
        if("#{input}" == "s" || "#{input}" == "S")
        printPrograms
        end
        if("#{input}" == "n" || "#{input}" == "N")
        $noConf = true
        end
        if("#{input}" == "q" || "#{input}" == "Q")
        $cont = false
        end
    end
end
end
